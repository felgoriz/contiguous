package com.contiguous.contiguous;
/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

        import android.app.IntentService;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.location.Location;
        import android.location.LocationListener;
        import android.location.LocationManager;
        import android.os.Bundle;
        import android.os.Looper;
        import android.preference.PreferenceManager;
        import android.support.v4.content.LocalBroadcastManager;
        import android.util.Log;

        import com.google.android.gms.gcm.GcmPubSub;
        import com.google.android.gms.gcm.GoogleCloudMessaging;
        import com.google.android.gms.iid.InstanceID;
        import com.google.android.gms.maps.model.LatLng;

        import org.apache.commons.io.IOUtils;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.IOException;
        import java.io.InputStream;
        import java.io.OutputStream;
        import java.net.HttpURLConnection;
        import java.net.MalformedURLException;
        import java.net.ProtocolException;
        import java.net.URL;

public class RegistrationIntentService extends IntentService {

    public static final String API_KEY = "AIzaSyBLm2IJwK6W0WDjBfUPNXsqSGexmF_2pOY";
    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private Location mLastLocation;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            // In the (unlikely) event that multiple refresh operations occur simultaneously,
            // ensure that they are processed sequentially.
            synchronized (TAG) {
                // [START register_for_gcm]
                // Initially this call goes out to the network to retrieve the token, subsequent calls
                // are local.
                // [START get_token]
                InstanceID instanceID = InstanceID.getInstance(this);
                final String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                // [END get_token]
                Log.i(TAG, "GCM Registration Token: " + token);

                // TODO: Implement this method to send any registration to your app's servers.
                //String random = UUID.randomUUID().toString().replace("-","");
               sendRegistrationToServer(token);

                // Subscribe to topic channels
                subscribeTopics(token);

                // You should store a boolean that indicates whether the generated token has been
                // sent to your server. If the boolean is false, send the token to your server,
                // otherwise your server should have already received the token.
                sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
                // [END register_for_gcm]

               /* LocationManager locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
                LocationListener locationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        locationUpdated(location, token.substring(0,6));
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                };

                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 120000, 5, locationListener);*/
            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
        /*if(Looper.myLooper() == null)
            Looper.prepare();
        Looper.loop();*/

    }

    private String getUsuario() {
        String userkey = "userkey";
        String ARCHIVO_PERSISTENCIA = "contagious.dat";
        SharedPreferences spref;
        spref = getApplicationContext().getSharedPreferences(
                ARCHIVO_PERSISTENCIA, Context.MODE_PRIVATE);
       return spref.getString(userkey, "NN");
        // TODO this should not be encode
    }

    protected void locationUpdated(Location location, String token) {
        //System.out.println("LOCATION_CHANGED!");
        try {
            mLastLocation = location;
            LatLng loc = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

            String result = BackendAPI.consumirBackendGET("http://contiguous-felgoriz.rhcloud.com/api/user/" + token + "/find");

            JSONObject jObj = new JSONObject();
            JSONArray array = new JSONArray();
            array.put(loc.longitude);
            array.put(loc.latitude);
            String url = "http://contiguous-felgoriz.rhcloud.com/api/user/"+token+"/update";
            if (result == null || "".equals(result) || "null".equals(result)) {
                jObj.put("_id", token);
                url="http://contiguous-felgoriz.rhcloud.com/api/user/save";
            }

            jObj.put("name", getUsuario());
            jObj.put("location", array);
            BackendAPI.consumirBackend(jObj, url);
            //BackendAPI.consumirBackend(obj, "");
            /**
             MapFragment mapFragment = (MapFragment) getFragmentManager()
             .findFragmentById(R.id.map);

             mapFragment.getMap().clear();
             mapFragment.getMap().setMyLocationEnabled(true);
             mapFragment.getMap().moveCamera(
             CameraUpdateFactory.newLatLngZoom(loc, 13));

             mapFragment.getMap().addMarker(
             new MarkerOptions().title(username)
             .snippet("Esta es tu posición").position(loc));
             */
        }
        catch(Exception e)
        {

        }
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        String message = "Tengo una idea de hola mundo prueba";
        String resp = sendMessageGCM(message);
        if(!resp.equals(""))
        {
            sendMessageServer("hackaton", resp, token, 4.0, 5.0);
        }
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        for (String topic : TOPICS) {
            GcmPubSub pubSub = GcmPubSub.getInstance(this);
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]


    public static String sendMessageGCM(String message)
    {
        String resp = "";
        try {
            JSONObject jGcmData = new JSONObject();
            JSONObject jData = new JSONObject();

            jData.put("message", message);

        // Where to send GCM message.
            jGcmData.put("to", "/topics/global");
            // What to send in GCM message.
            jGcmData.put("data", jData);

            // Create connection to send GCM Message request.
            URL url = new URL("https://android.googleapis.com/gcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Authorization", "key=" + API_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            // Send GCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jGcmData.toString().getBytes());
            outputStream.close();

            // Read GCM response.
            InputStream inputStream = conn.getInputStream();
            resp = IOUtils.toString(inputStream);
            inputStream.close();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resp;
    }

    public static void sendMessageServer(String random, String resp, String message, Double latitude, Double longitud)
    {
        try {
            JSONObject messageId = new JSONObject(resp);
            JSONObject jObj = new JSONObject();
            JSONArray arr = new JSONArray();
            arr.put(longitud);
            arr.put(latitude);



            jObj.put("_id", messageId.get("message_id"));
            jObj.put("author", random);
            jObj.put("text", message);
            jObj.put("location", arr);
            BackendAPI.consumirBackend(jObj, "http://contiguous-felgoriz.rhcloud.com/api/idea/save");
        }
        catch(Exception e)
        {

        }
    }
}