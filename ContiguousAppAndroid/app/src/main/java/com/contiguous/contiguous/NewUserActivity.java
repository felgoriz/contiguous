package com.contiguous.contiguous;

import com.contiguous.contiguous.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

public class NewUserActivity extends Activity{
    /**private static Twitter tw;
     private static RequestToken requestToken;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_user);


    }
    /**
     * Registro de datos
     */
    public void loggear (View view){
        System.out.println("Loggear");
        SharedPreferences spref = getApplicationContext().getSharedPreferences(MainActivity.ARCHIVO_PERSISTENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spref.edit();

        EditText usernm   = (EditText)findViewById(R.id.txtuser);
        String usernmst = usernm.getText().toString();

        EditText pw   = (EditText)findViewById(R.id.txtpw);
        String pwst = pw.getText().toString();

        EditText pw2   = (EditText)findViewById(R.id.txtpw2);
        String pw2st = pw2.getText().toString();


        if (pwst.equals("") || pw2st.equals("") || usernmst.equals("") || pwst == null || pw2st == null || usernmst == null)
        {
            Toast.makeText(getApplicationContext(), "Por favor llena todos los campos", Toast.LENGTH_SHORT).show();
        }
        else if (!pw2st.equals(pwst))
        {
            Toast.makeText(getApplicationContext(), "Tus contraseñas no coinciden", Toast.LENGTH_SHORT).show();
        }
        else{
            editor.putString(getString(R.string.userkey), usernmst);
            editor.commit();

            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }
    /**
     * Integracion con Twitter
     * @param view
     */
    /**public void askOAuth(View view) {
     //TODO check social login
     ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
     configurationBuilder.setOAuthConsumerKey(ConstTW.CONSUMER_KEY);
     configurationBuilder.setOAuthConsumerSecret(ConstTW.CONSUMER_SECRET);
     Configuration configuration = configurationBuilder.build();
     tw = new TwitterFactory(configuration).getInstance();

     try {
     requestToken = tw.getOAuthRequestToken(ConstTW.CALLBACK_URL);
     this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL())));
     } catch (TwitterException e) {
     e.printStackTrace();
     }
     }
     */


    //TODO onAction when signed in
}
