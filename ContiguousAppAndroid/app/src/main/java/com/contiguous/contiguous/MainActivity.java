package com.contiguous.contiguous;

/**
 * Created by david on 4/07/15.
 /**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.Random;


public class MainActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener, OnClickListener {
    private static final String URL="file:///android_asset/index.html";
    public final static String userkey = "userkey";
    public final static String ARCHIVO_PERSISTENCIA = "contagious.dat";
    public SharedPreferences spref;
    public String username;


    private WebView mWebView;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;

    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private Button buttonSend;

    Intent intent;
    private boolean side = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        if(!cargado()){
            Intent intent = new Intent(getApplicationContext(), NewUserActivity.class);
            startActivity(intent);
        }

        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.setBackgroundColor(Color.TRANSPARENT);
        mWebView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());



        /*mWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                String javascript = "javascript: addIdea('dfe');";
                view.loadUrl(javascript);
                mWebView.setBackgroundColor(Color.TRANSPARENT);
                mWebView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
            }
        });*/

        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.addJavascriptInterface(this, "Android");

        refreshWebView();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private void refreshWebView() {
        mWebView.loadUrl(URL);
    }

    public void recibirIdea(final String id) {

        new ApiTask().execute(id);

    }

    class ApiTask extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... params) {
            try {
                String id = params[0];
                String message = id;
                String resp = RegistrationIntentService.sendMessageGCM(message);
                if(resp != null)
                {
                    RegistrationIntentService.sendMessageServer("global", resp, message, 4.6897144, -74.0710285);
                }
                System.out.println("IDEA: " + id);
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
            return "";
        }

        protected void onPostExecute(String feed) {
            // TODO: check this.exception
            // TODO: do something with the feed
        }
    }


    public void misideas(View view) {
        Intent intent = new Intent(getApplicationContext(),
                MisIdeasActivity.class);
        startActivity(intent);
    }
    public void enviarIdea(View view) {
        // get prompts.xml view
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Enviar Idea:");
        alert.setMessage("¿Cuál es tu idea?");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                recibirIdea(value);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();

    }

    private boolean cargado() {
        spref = getApplicationContext().getSharedPreferences(
                ARCHIVO_PERSISTENCIA, Context.MODE_PRIVATE);
        username = spref.getString(userkey, "NN");
        // TODO this should not be encoded
        if (username == "NN") {
            return false;
        } else
            return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    public void addIdeaWebView(String id){
        String javascript = "javascript: addIdea('"+id+"');";
        mWebView.loadUrl(javascript);
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void inspectIdea(String rid) {
        Toast.makeText(getApplicationContext(), "ID: " + rid, Toast.LENGTH_SHORT).show();
        // TODO generate Dialog from idea

        Dialog mDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        mDialog.setContentView(R.layout.view_idea_dialog);
        mDialog.show();

    }

    public void comentarIdea(View view){

        String javascript="javascript: addIdea('dfe');";
        mWebView.loadUrl(javascript);
    }

    public void mensajear(View view){

        String javascript="javascript: addIdea('dfe');";
        mWebView.loadUrl(javascript);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}