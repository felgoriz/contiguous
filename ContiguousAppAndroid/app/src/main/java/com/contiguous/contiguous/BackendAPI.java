package com.contiguous.contiguous;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by david on 4/07/15.
 */
public class BackendAPI {

    public static void consumirBackend(JSONObject obj, String url_str)
    {
        try {


            // Create connection to send GCM Message request.
            URL url = new URL(url_str);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            // Send GCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(obj.toString().getBytes());

            // Read GCM response.
            InputStream inputStream = conn.getInputStream();
            String result = IOUtils.toString(inputStream);
            System.out.println(result);
        }
        catch(Exception e)
        {

        }
    }

    public static String consumirBackendGET(String url_str)
    {
        try {


            // Create connection to send GCM Message request.
            URL url = new URL(url_str);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // Read GCM response.
            InputStream inputStream = conn.getInputStream();
            String result = IOUtils.toString(inputStream);
            System.out.println(result);
            return result;
        }
        catch(Exception e)
        {

        }
        return null;
    }
}
