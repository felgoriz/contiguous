var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var IdeaSchema = new Schema({
  _id: String,
  author: {
    type: String,
    required: 'Ingrese el id del usuario'
  },
  text: {
    type: String,
    required: 'Ingrese el texto de la idea'
  },
  location: {
    type: [Number],
    index: '2dsphere'
  },
  created: {
    type: Date,
    default: Date.now
  }
});

mongoose.model('Idea', IdeaSchema);