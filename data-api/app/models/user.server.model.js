var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var UserSchema = new Schema({
  _id: String,
  name: {
    type: String,
    required: 'user name is required'
  },
  location: {
    type: [Number],
    index: '2dsphere'
  },
  created: {
    type: Date,
    default: Date.now
  }
});

mongoose.model('User', UserSchema);