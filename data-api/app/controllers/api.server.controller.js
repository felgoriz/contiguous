/* global exports */

var Idea = require('mongoose').model('Idea'),
  User = require('mongoose').model('User');

var http = require("http");

var getErrorMessage = function (err) {
  var message = '';
  if (err.code) {
    switch (err.code) {
      case 11000:
      case 11001:
        message = 'Already exists';
        break;
      default:
        message = 'Something happened';
    }
  } else {
    for (var errName in err.errors) {
      if (err.errors[errName].message)
        message = err.errors[errName].message;
    }
  }
  return message;
};

var pushMessage = function (usersId, ideaId) {
  var options = {
    hostname: 'android.googleapis.com',
    port: 80,
    path: '/gcm/send',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'key=AIzaSyBLm2IJwK6W0WDjBfUPNXsqSGexmF_2pOY'
    }
  };
  var payload = '{"to": "/topics/global", "data": {"message": {"users":[' + usersId + '],"topic":'
    + ideaId + '}}}';
  var req = http.request(options, function (res) {
    res.setEncoding('utf8');
    res.on('data', function (body) {
      console.log(body);
    });
  });
  req.on('error', function (e) {
    console.error('Problem with request: ' + e.message);
  });
  req.write(payload);
  req.end();
};


//===========================================
// Idea methods
//===========================================

exports.saveIdea = function (req, res) {
  var idea = new Idea(req.body);
  idea.save(function (err) {
    if (err) {
      var message = getErrorMessage(err);
      return res.json({message: message});
    }
    pushMessageToContiguousUsers(idea.location, idea._id);
    res.json({message: 'Idea saved'});
  });
};

exports.listIdeas = function (req, res) {
  Idea.find({}, function (err, ideas) {
    if (err) {
      res.send(err);
    } else {
      res.json(ideas);
    }
  });
};

exports.findIdea = function (req, res) {
  var id = req.params.id;
  Idea.findById(id, function (err, idea) {
    if (err) {
      res.send(err);
    } else {
      res.json(idea);
    }
  });
};

exports.updateIdea = function (req, res) {
  var id = req.params.id;
  Idea.findById(id, function (err, idea) {
    if (err) {
      res.send(err);
    }
    idea.author = req.body.author;
    idea.text = req.body.text;
    idea.location = req.body.location;
    idea.save(function (err) {
      if (err) {
        res.send(err);
      }
      res.json({message: 'Idea updated'});
    });
  });
};

exports.deleteIdea = function (req, res) {
  var id = req.params.id;
  Idea.findByIdAndRemove(id, function (err) {
    if (err) {
      res.send(err);
    }
    res.json({message: 'Idea deleted'});
  });
};


//===========================================
// User methods
//===========================================

var pushMessageToContiguousUsers = function (ideaLocation, ideaId) {
  User.find({location: {'$near': {'$maxDistance': 10, '$geometry': {type: 'Point',
          coordinates: ideaLocation
        }}}}, function (err, users) {
    if (err) {
      console.error(err);
    } else {
      var usersId = [];
      for (var i = 0; i < users.length; i++) {
        usersId.push('"' + users[i]._id + '"');
      }
      pushMessage(usersId, '"' + ideaId + '"');
    }
  });
};

exports.contiguousUsers = function (req, res) {
  var location = req.body.location;
  User.find({location: {'$near': {'$maxDistance': 10, '$geometry': {type: 'Point',
          coordinates: location
        }}}}, function (err, users) {
    if (err) {
      res.send(err);
    } else {
      res.json(users);
    }
  });
};

exports.saveUser = function (req, res) {
  var user = new User(req.body);
  user.save(function (err) {
    if (err) {
      var message = getErrorMessage(err);
      return res.json({message: message});
    }
    res.json({message: 'User saved'});
  });
};

exports.listUsers = function (req, res) {
  User.find({}, function (err, users) {
    if (err) {
      res.send(err);
    } else {
      res.json(users);
    }
  });
};

exports.findUser = function (req, res) {
  var id = req.params.id;
  User.findById(id, function (err, user) {
    if (err) {
      res.send(err);
    } else {
      res.json(user);
    }
  });
};

exports.updateUser = function (req, res) {
  var id = req.params.id;
  User.findById(id, function (err, user) {
    if (err) {
      res.send(err);
    }
    user.name = req.body.name;
    user.location = req.body.location;
    user.save(function (err) {
      if (err) {
        res.send(err);
      }
      res.json({message: 'User updated'});
    });
  });
};

exports.updateUserLocation = function (req, res) {
  var id = req.params.id;
  User.findById(id, function (err, user) {
    if (err) {
      res.send(err);
    }
    user.location = req.body.location;
    user.save(function (err) {
      if (err) {
        res.send(err);
      }
      res.json({message: 'User location updated'});
    });
  });
};

exports.deleteUser = function (req, res) {
  var id = req.params.id;
  User.findByIdAndRemove(id, function (err) {
    if (err) {
      res.send(err);
    }
    res.json({message: 'User deleted'});
  });
};