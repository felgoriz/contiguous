/* global module */

var api = require('../../app/controllers/api.server.controller');
module.exports = function (app) {
  app.route('/api/idea/save').post(api.saveIdea);
  app.route('/api/idea/list').get(api.listIdeas);
  app.route('/api/idea/:id/find').get(api.findIdea);
  app.route('/api/idea/:id/update').put(api.updateIdea);
  app.route('/api/idea/:id/delete').delete(api.deleteIdea);
  app.route('/api/user/save').post(api.saveUser);
  app.route('/api/user/list').get(api.listUsers);
  app.route('/api/user/:id/find').get(api.findUser);
  app.route('/api/user/:id/update').put(api.updateUser);
  app.route('/api/user/:id/location/update').put(api.updateUserLocation);
  app.route('/api/user/:id/delete').delete(api.deleteUser);
  app.route('/api/user/contiguous').post(api.contiguousUsers);
};