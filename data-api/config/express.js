/* global module */

var config = require('./config'),
  express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override');

module.exports = function () {
  var app = express();
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  require('../app/routes/api.server.routes.js')(app);
  return app;
};