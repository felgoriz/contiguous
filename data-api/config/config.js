/* global module */

var db_name = 'contiguous';
var mongodb_connection_string = 'mongodb://localhost/'+ db_name;

// OPENSHIFT CONFIGURATION
// var mongodb_connection_string = 'mongodb://127.0.0.1:27017/' + db_name;
// if(process.env.OPENSHIFT_MONGODB_DB_URL){
//   mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + db_name;
// }

module.exports = {
  db: mongodb_connection_string
};