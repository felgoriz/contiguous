/* global module */

var mongoose = require('./config/mongoose'),
  express = require('./config/express');

var db = mongoose();
var app = express();

var host = 'localhost';
var port = 3000;

// OPENSHIFT CONFIGURATION
// var host = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
// var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

app.listen(port, host);
module.exports = app;
console.log('Server running at http://' + host + ':' + port + '/');